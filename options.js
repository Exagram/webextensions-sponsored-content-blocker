console.log('options');

function defaultSitesConfigFactory() {
	return [
		{
			url: 'facebook.com',
			checked: true
		},
		{
			url: 'reddit.com',
			checked: true
		}
	];
}

function saveOptions(e) {
	const documentTags = document.querySelectorAll("#sites input[type=checkbox]");
	const documentsToSave = [];
	documentTags.forEach((documentTag) => {
		documentsToSave.push({
			checked: documentTag.checked,
			url: documentTag.value
		});
	});
	browser.storage.sync.set({
		sites: documentsToSave
	});
	e.preventDefault();
}

function restoreOptions() {
	var getPromise = browser.storage.sync.get({
		sites: null
	});
	getPromise.then((get) => {
		let sites = get.sites;
		if (sites === null) {
			sites = defaultSitesConfigFactory();
		}
		const sitesDiv = document.querySelector("#sites");
		sites.forEach((site) => {
			const checked = site.checked ? 'checked' : '';
			sitesDiv.insertAdjacentHTML('beforeend', `<label><input type="checkbox" value="${site.url}" ${checked} />${site.url}</label>`);
		});
	});
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
